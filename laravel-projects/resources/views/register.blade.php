@extends('layouts.master')

@section('title', 'Register')

@section('content')
<form action="/kirim" method = "post">
    @csrf
    <label for="">First name:</label><br><br>
    <input type="text" name = "firstName"><br><br>
    <label for="">Last name:</label><br><br>
    <input type="text" name = "lastName"><br><br>
    <label for="radio">Gender:</label><br><br>
    <input type="radio" name = "gender" value = "Male">Male <br>
    <input type="radio" name = "gender" value = "Female">Female <br>
    <input type="radio" name = "gender" value = "Other">Other <br><br>
    <label for="">Nationality:</label> <br>
    <select name="Nationality" id="Nationality">
        <option value="Indonesia">Indonesian</option>
        <option value="Singaporean">Singaporean</option>
        <option value="Malaysia">Malaysian</option>
        <option value="Australian">Australian</option>
    </select>
    <br><br>
    <label for="checkbox">Language Spoken:</label><br><br>
    <input type="checkbox" name="Language" id="Indonesian" value = "Bahasa Indonesia"> Bahasa Indonesia <br>
    <input type="checkbox" name="Language" id="English" value ="English"> English <br>
    <input type="checkbox" name="Language" id="Other" value = "Other"> Other <br><br>
    <label for="">Bio:</label><br><br>
    <textarea name="bio" id="Bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Kirim">
</form>
@endsection

{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form action="/kirim" method = "post">
    @csrf
    <label for="">First name:</label><br><br>
    <input type="text" name = "firstName"><br><br>
    <label for="">Last name:</label><br><br>
    <input type="text" name = "lastName"><br><br>
    <label for="radio">Gender:</label><br><br>
    <input type="radio" name = "gender" value = "Male">Male <br>
    <input type="radio" name = "gender" value = "Female">Female <br>
    <input type="radio" name = "gender" value = "Other">Other <br><br>
    <label for="">Nationality:</label> <br>
    <select name="Nationality" id="Nationality">
        <option value="Indonesia">Indonesian</option>
        <option value="Singaporean">Singaporean</option>
        <option value="Malaysia">Malaysian</option>
        <option value="Australian">Australian</option>
    </select>
    <br><br>
    <label for="checkbox">Language Spoken:</label><br><br>
    <input type="checkbox" name="Language" id="Indonesian" value = "Bahasa Indonesia"> Bahasa Indonesia <br>
    <input type="checkbox" name="Language" id="English" value ="English"> English <br>
    <input type="checkbox" name="Language" id="Other" value = "Other"> Other <br><br>
    <label for="">Bio:</label><br><br>
    <textarea name="bio" id="Bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Kirim">
</form>
</body>
</html> --}}