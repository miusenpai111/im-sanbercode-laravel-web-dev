TUGAS DATABASE

1. Membuat database 
Buatlah database dengan nama �myshop�.
CREATE DATABASE myshop;
2. Membuat table di dalam database 
Users
CREATE�TABLE�`myshop`.`users`�(`id`�INT�NOT�NULL�AUTO_INCREMENT�,�`name`�VARCHAR(255)�NULL�,�`email`�VARCHAR(255)�NULL�,�`password`�VARCHAR(255)�NULL�,�PRIMARY�KEY�(`id`))�ENGINE�=�InnoDB;
Categories
CREATE�TABLE�`myshop`.`categories`�(`id`�INT�NOT�NULL�AUTO_INCREMENT�,�`name`�VARCHAR(255)�NULL�,�PRIMARY�KEY�(`id`))�ENGINE�=�InnoDB;
Items
CREATE�TABLE�`myshop`.`items`�(`id`�INT�NOT�NULL�AUTO_INCREMENT�,�`name`�VARCHAR(255)�NULL�,�`description`�VARCHAR(255)�NULL�,�`price`�INT�NULL�,�`stock`�INT�NULL�,�`category_id`�INT�NOT�NULL�,�PRIMARY�KEY�(`id`),�INDEX�`category_id_fk`�(`category_id`))�ENGINE�=�InnoDB;

FK
ALTER�TABLE�`items`�ADD�FOREIGN�KEY�(`category_id`)�REFERENCES�`categories`(`id`)�ON�DELETE�RESTRICT�ON�UPDATE�RESTRICT;
3. Memasukkan data pada table 
Users
Bagian Atas Formulir
INSERT�INTO�`users`�(`id`,�`name`,�`email`,�`password`)�VALUES�(NULL,�'John Doe',�'john@doe.com',�'john123'),�(NULL,�'Jane Doe',�'jane@doe.com',�'jenita123')Bagian Bawah Formulir
Categories
INSERT�INTO�`categories`�(`id`,�`name`)�VALUES�(NULL,�'gadget'),�(NULL,�'cloth'),�(NULL,�'men'),�(NULL,�'women'),�(NULL,�'branded')
Items
INSERT�INTO�`items`�(`id`,�`name`,�`description`,�`price`,�`stock`,�`category_id`)�VALUES�(NULL,�'Sumsang b50',�'hape keren dari merek sumsang',�'4000000',�'100',�'1'),�(NULL,�'Uniklooh',�'baju keren dari brand ternama',�'500000',�'50',�'2'),�(NULL,�'IMHO Watch',�'jam tangan anak yang jujur banget',�'2000000',�'10',�'1')
4. Mengambil data dari database 
a) mengambil data users 
SELECT�name,�email�FROM�users;
b) mengambil data items 
* Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

SELECT�*�FROM�items�WHERE�price�>�1000000;

* Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci �uniklo�, �watch�, atau �sang� (pilih salah satu saja).
�SELECT�*�FROM�items�WHERE�name�LIKE�'%uniklo%';
c)  Menampilkan data items join dengan kategori 
SELECT�items.name,�items.description,�items.price,�items.stock,�items.category_id,�categories.name�FROM�items�JOIN�categories�ON�categories.id�=�items.category_id;
5. Mengubah database
UPDATE�`items`�SET�`price`�=�'2500000'�WHERE�`items`.`id`�=�1
