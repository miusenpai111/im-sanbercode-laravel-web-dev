<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/register', [AuthController::class, 'register']);
Route::post('/kirim', [AuthController::class, 'kirim']);


Route::get('/master', function(){
    return view('layouts.master');
});

Route::get('/table', function(){
    return view('layouts.pages.tables');
});


Route::get('/data-tables', function(){
    return view('layouts.pages.data-table');
});


//CRUD CAST 

//c --> create
route::get('/cast/create', [CastController::class, 'create']);

route::post('/cast', [CastController::class, 'store']);

//r --> read
route::get('/cast', [CastController::class, 'index']);

route::get('/cast/{id}', [CastController::class, 'show']);

//u --> update
route::get('/cast/{id}/edit', [CastController::class, 'edit']);

route::put('/cast/{id}', [CastController::class, 'update']);

//D --> delete
route::delete('/cast/{id}', [CastController::class, 'destroy']);









// Route::get('/welcome', [AuthController::class, 'ketiga']);