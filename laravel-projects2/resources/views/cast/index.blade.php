@extends('layouts.master')
@section('title', 'All cast')

@section('content')
    <a href="/cast/create" class = "btn btn-primary my-3"> Add cast</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col"> </th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $keys => $cast)
                <tr>
                    <th scope="row">{{ $keys + 1 }}</th>
                    <td>{{ $cast->nama }}</td>
                    <td><a href="/cast/{{ $cast->id }}" class="btn btn-info btn-sm">Detail</a></td>
                    <td><a href="/cast/{{ $cast->id }}/edit" class="btn btn-warning btn-sm">Update</a></td>
                    <td>
                        <form action="/cast/{{ $cast->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>
                        Cast KOSONG
                    </td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
