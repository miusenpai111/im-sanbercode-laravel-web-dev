@extends('layouts.master')
@section('title', 'Add cast')

@section('content')
    <form action="/cast" method="post">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name = "name" @error('name') is-invalid @enderror>
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control" name = "umur" @error('umur') is-invalid @enderror>
            @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
        <div class="form-group">
            <label>Biography</label>
            <textarea class ="form-control" name="bio" cols="30" rows="10" @error('bio') is-invalid @enderror></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
