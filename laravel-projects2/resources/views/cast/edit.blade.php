@extends('layouts.master')
@section('title', 'Edit cast')

@section('content')
    <form action="/cast/{{$cast->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name = "name" @error('name') is-invalid @enderror value="{{$cast->nama}}">
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control" name = "umur" @error('umur') is-invalid @enderror value="{{$cast->umur}}">
            @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
        <div class="form-group">
            <label>Biography</label>
            <textarea class ="form-control" name="bio" cols="30" rows="10" @error('bio') is-invalid @enderror>{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
