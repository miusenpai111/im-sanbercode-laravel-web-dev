@extends('layouts.master')
@section('title', 'Cast detail ')

@section('content')
   <h1 class="text-primary">{{$cast->nama}}</h1>
   <p>{{$cast->umur}}</p>
   <p>{{$cast->bio}}</p>
   <a href="/cast" class="btn btn-primary btn-sm my-3">Back</a>
@endsection
