<?php
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$hewan = new Animal("Shaun", 4, "No");
echo "Name: ". $hewan->name. "<br>";
echo "Legs: ". $hewan->legs. "<br>";
echo "Cold-blooded: ". $hewan->coldBlooded. "<br> <br>";

$kodok = new Kodok("Buduk", 4, "No");
echo "Name: ". $kodok->name. "<br>";
echo "Legs: ". $kodok->legs. "<br>";
echo "Cold-blooded: ". $kodok->coldBlooded. "<br>";
echo "Jump: ". $kodok->jump. "<br> <br>";

$kera = new Kera("Kera Sakti", 2, "No");
echo "Name: ". $kera->name. "<br>";
echo "Legs: ". $kera->legs. "<br>";
echo "Cold-blooded: ". $kera->coldBlooded. "<br>";
echo "Jump: ". $kera->yell. "<br> <br>";
?>




