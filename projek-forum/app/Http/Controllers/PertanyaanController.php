<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategori;
use App\Models\pertanyaan;
use File;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {   
        $pertanyaan = pertanyaan::get();
        return view('pertanyaan.index', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori=kategori::get();

        return view('pertanyaan.create', ['kategori'=> $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'required|image|mimes:png,jpg,jpeg',
        ]);    

      
  
        $fileName = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('image'), $fileName);



            $pertanyaan = new pertanyaan;

            $pertanyaan->judul = $request->judul;
            $pertanyaan->content = $request->content;
            $pertanyaan->kategori_id = $request->kategori_id;
            $pertanyaan->gambar = $fileName;

            $pertanyaan->save();
     
            return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = pertanyaan::find($id);

        return view('pertanyaan.detail', ['pertanyaan' =>$pertanyaan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = pertanyaan::find($id);
        $kategori=kategori::get();
        
        return view('pertanyaan.update', ['pertanyaan'=>$pertanyaan, 'kategori'=>$kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'image|mimes:png,jpg,jpeg',
        ]);    

        $pertanyaan = pertanyaan::find($id);
        
        if ($request->has('gambar')) {
            $path = 'image/';
            File::delete($path. $pertanyaan->gambar);

            $fileName = time().'.'.$request->gambar->extension();  
   
            $request->gambar->move(public_path('image'), $fileName);
            
            $pertanyaan->gambar = $fileName;

            $pertanyaan->save();
        }
        $pertanyaan->judul = $request['judul'];
        $pertanyaan->content = $request['content'];
        $pertanyaan->kategori_id = $request['kategori_id'];
 
        $pertanyaan->save();

        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = pertanyaan::find($id);

        $path = 'image/';
        File::delete($path. $pertanyaan->gambar);

        $pertanyaan->delete();

        return redirect('/pertanyaan');
    }
}
