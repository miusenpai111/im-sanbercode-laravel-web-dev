<?php

namespace App\Http\Controllers;
//Database
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function create(){
        return view('kategori.tambah');
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'nama_kategori' => 'required',
        ]);    

        DB::table('kategori')->insert([
            'nama_kategori' => $request['nama_kategori'],
        ]);


        return redirect('/kategori');
    }

    public function read(){

        $kategori = DB::table('kategori')->get();
        //dd($kategori);

        return view('kategori.tampil', ['kategori' => $kategori]);
    }

    public function edit($id){
        $kategori = DB::table('kategori')->where('id', $id)->first();

        return view('kategori.edit', ['kategori' => $kategori]);
    }

    public function update(Request $request, $id){
        $validated = $request->validate([
            'nama_kategori' => 'required',
        ]);    

        DB::table('kategori')
            ->where('id', $id)
            ->update(
                ['nama_kategori' => $request->nama_kategori]
            );
            return redirect('/kategori');
    }

    public function destroy($id){
        DB::table('kategori')->where('id',$id)->delete();

        return redirect('/kategori');
    }
}
