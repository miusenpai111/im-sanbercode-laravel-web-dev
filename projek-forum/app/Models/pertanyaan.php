<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pertanyaan extends Model
{
    use HasFactory;

    protected $table = 'pertanyaan';

    protected $fillable = ['judul', 'content', 'gambar', 'kategori_id'];

    public function kategori(){
        return $this->belongsTo(kategori::class, 'kategori_id');
    }
}
