@extends('layout.master')

@section('main-menu')
    <h1>KATEGORI</h1>
@endsection
@section('judul')
    <h1>EDIT KATEGORI</h1>
@endsection

@section('content')
    <form action="/kategori/{{ $kategori->id }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="exampleInputEmail1">Tambah Nama Kategeori</label>
            <input type="text" name="nama_kategori" value="{{ $kategori->nama_kategori }}" class="form-control" placeholder="Masukkan kategori">
        </div>
        @error('nama_kategori')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
