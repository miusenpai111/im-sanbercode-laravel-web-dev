@extends('layout.master')

@section('main-menu')
    <h1>KATEGORI</h1>
@endsection
@section('judul')
    <h1>LIST KATEGORI</h1>
@endsection

@section('content')
    <a href="/kategori/create" class="btn btn-primary btn-sm">Tambah Kategori</a>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama Kategori</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($kategori as $key => $value)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $value->nama_kategori }}</td>
                    <td>
                        <form action="/kategori/{{ $value->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/kategori/{{ $value->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Tidak ada data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
