@extends('layout.master')

@section('main-menu')
    <h1>Profile</h1>
@endsection
@section('judul')
    <h1>Update Profile</h1>
@endsection

@section('content')

    <form action="/profile/{{ $detailProfile->id }}" method="POST">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="exampleInputEmail1">Nama User</label>
            <input type="text"  value="{{ $detailProfile->user->name }}" class="form-control" placeholder="Masukkan umur" disabled>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Email user</label>
            <input type="text"  value="{{ $detailProfile->user->email }}" class="form-control" placeholder="Masukkan umur" disabled>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Umur</label>
            <input type="number" name="umur" value="{{ $detailProfile->umur }}" class="form-control" placeholder="Masukkan umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="exampleInputEmail1">Alamat</label>
            <textarea name="alamat"  class="form-control" cols="30" rows="10">{{ $detailProfile->alamat }}</textarea>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label for="exampleInputEmail1">Biodata</label>
            <textarea name="biodata"  class="form-control" cols="30" rows="10">{{ $detailProfile->biodata }}</textarea>
        </div>
        @error('biodata')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
