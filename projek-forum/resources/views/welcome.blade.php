@extends('layout.master')

@section('main-menu')
    <h1>Selamat datang di forum tanya jawab !</h1>
@endsection

@section('judul')
<h1>Welcome</h1>
@endsection

@section('content')
<h4>Selamat datang di forum tanya jawab </h4>
<p>Terdapat beberapa aturan saat mengikuti forum ini:</p>
<ol>
    <li>Bertanyalah dengan sopan</li>
    <li>Bersenang-senang</li>
</ol>

@endsection