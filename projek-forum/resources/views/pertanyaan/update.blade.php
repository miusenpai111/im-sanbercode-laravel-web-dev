@extends('layout.master')

@section('main-menu')
    <h1>Pertanyaan</h1>
@endsection
@section('judul')
    <h1>Edit PERTANYAAN</h1>
@endsection

@section('content')
    <form action="/pertanyaan/{{ $pertanyaan->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="exampleInputEmail1">Tambah Judul</label>
            <input type="text" name="judul" class="form-control" value="{{ $pertanyaan->judul }}"
                placeholder="Masukkan Judul">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label>Isi Content Pertanyaan</label>
        <textarea name="content" class="form-control" cols="30" rows="10">{{ $pertanyaan->content }}</textarea>
        @error('content')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Tambah Gambar</label>
            <input type="file" name="gambar" class="form-control" placeholder="Masukkan Gambar">
        </div>
        @error('gambar')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Isi Kategori</label>
            <select name="kategori_id" class="form-control" id="">
                <option value="">--Pilih Kategori--</option>
                @forelse ($kategori as $item)
                    @if ($item->id === $pertanyaan->kategori_id)
                        <option value="{{ $item->id }}" selected>{{ $item->nama_kategori }}</option>
                    @else
                    <option value="{{ $item->id }}">{{ $item->nama_kategori }}</option>

                    @endif
                @empty
                <option value="">Tidak ada kategori</option>
                @endforelse
            </select>
        </div>
        @error('kategori_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
