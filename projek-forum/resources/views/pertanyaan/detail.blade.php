@extends('layout.master')

@section('main-menu')
    <h1>Halaman Forum</h1>
@endsection
@section('judul')
    <h1>Detail</h1>
@endsection

@section('content')
    
        <img class="card-img-top" src="{{ asset('image/' . $pertanyaan->gambar) }}" alt="Card image cap">
        <div class="card-body">
            <h3>{{ $pertanyaan->judul }}</h3>
            <p class="card-text">{{ $pertanyaan->content }}</p>
            <h5>{{ $pertanyaan->kategori->nama_kategori }}</h5>
            <a href="/pertanyaan" class="btn btn-primary btn-sm">Kembali</a>
        </div>

@endsection