@extends('layout.master')

@section('main-menu')
    <h1>Halaman Forum</h1>
@endsection
@section('judul')
    <h1>Selamat Bertanya</h1>
@endsection


@section('content')

@auth
<a href="/pertanyaan/create" class="btn btn-primary btn-sm mb-4">Tambah pertanyaan</a>
@endauth
<div class="row">
@forelse ($pertanyaan as $item)
<div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('image/' . $item->gambar) }}" alt="Card image cap">
                    <div class="card-body">
                        <h3>{{ $item->judul }}</h3>
                        <p class="card-text">{{ Str::limit($item->content, 50) }}</p>
                        <a href="/pertanyaan/{{ $item->id }}" class="btn btn-primary btn-block btn-sm">Detail</a>
                        @auth
                        
                        <div class="row mt-2">
                            <div class="col">
                                <a href="/pertanyaan/{{ $item->id }}/edit"
                                    class="btn btn-warning  btn-block btn-sm">Edit</a>
                            </div>

                            <div class="col">
                                <form action="pertanyaan/{{ $item->id }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                                </form>
                            </div>
                        </div>
                        @endauth
                    </div>
                </div>
            </div>

        @empty
            Tidak ada postingan
        @endforelse



    </div>
@endsection
