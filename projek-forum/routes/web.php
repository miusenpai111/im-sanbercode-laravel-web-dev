<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController:: class, 'utama']);

// Route::get('/master', function(){
//     return view('layout.master');
// });

Route::get('/forum', function () {
    return view('halaman.forum');
});



Route::middleware(['auth'])->group(function () {
    //CRUD Kategori
    
    //create
    //Form tambah kategori
    Route::get('/kategori/create', [KategoriController::class,'create']);
    //Kirim tambah ke database
    Route::post('/kategori', [KategoriController::class,'store']);
    
    //Read
    //Tampil semua data 
    Route::get('/kategori', [KategoriController::class,'read']); //read
    
    //Update
    Route::get('/kategori/{kategori_id}/edit', [KategoriController::class,'edit']);
    //Update data ke database berdasarkan id 
    Route::put('/kategori/{kategori_id}', [KategoriController::class,'update'] );
    
    //Delete
    //Delete berdasarkan id 
    Route::delete('/kategori/{kategori_id}',  [KategoriController::class,'destroy']);

    //Profile
Route::resource('profile', ProfileController::class)->only(['index', 'update']);


});



//CRUD Pertanyaan
Route::resource('pertanyaan', PertanyaanController::class);

Auth::routes();


