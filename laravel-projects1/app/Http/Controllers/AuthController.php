<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function register(){
        return view('register');
    }

    public function kirim(Request $request){
        //dd($request->all());

        $namaDepan = $request['firstName'];
        $namaBelakang = $request['lastName'];
        $biografi = $request['bio'];

        return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }



}
